// On integre restify
var restify = require('restify');
var assert = require('assert');
// On integre redis
var redis = require('redis');

// On cree le serveur
var server = restify.createServer({
  name: 'server',
  version: '1.0.0'
});
// On cree le client
var client = redis.createClient({
  url: 'http://localhost:3000',
  version: '1.0.0'
}); 

// Action du serveur si une requete de type POST avec deux parametres est reçue
server.post('/:key/:value', function (req, res, next) {

  // On traite la requete
  client.set(req.params.key, req.params.value, function(err){
    console.log('Enregistrement de la valeur %s pour la cle %s', req.params.value, req.params.key);
  });
  // On confirme la reception de la requete
  res.send(200);
  return next();
});

// Action du serveur si une requete de type GET avec un parametre est reçue
server.get('/:key', function (req, res, next) {
  client.get(req.params.key, function(err, res){
    // Traitement
    console.log('Valeur pour la cle %s: %s', req.params.key, res);
  });
  // Confirmation
  res.send(200);
  return next();
});

// Action du serveur si une requete de type DELETE avec un parametre est reçue
server.del('/:key', function(req, res, next) {
  // Traitement
  client.del(req.params.key, function(err, reply){
    console.log('La valeur pour la cle %s a été supprimée', req.params.key);
  });
  // Confirmation
  res.send(200);
  return next();
});

// On avertit que le serveur a bien reussi a se connecter au port
server.listen(3000, function () {
  console.log('Serveur %s connecte a %s', server.name, server.url);
});



