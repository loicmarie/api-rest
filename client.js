// On integre restify
var restify = require('restify');
var assert = require('assert');

// On cree le client
var client = restify.createJsonClient({
  url: 'http://localhost:3000',
  version: '1.0.0'
});

// On envoie la requete POST
client.post('/30/test', function (err, req, res, obj) {

  assert.ifError(err);
  // On envoie la requete GET
  client.get('/30', function (err2, req2, res2, obj2) {
    assert.ifError(err2);
      // On envoie la requete DELETE
      client.del('/30', function(err3, req3, res3, obj3) {
        assert.ifError(err3);
      });
  });
});
