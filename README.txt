***********************
*****  API REST  ******
***********************

Cette application utilise NodeJS v0.10.37 et Redis v2.8.4
Aucun choix stratégique n'a été effectué vis à vis des versions, celles-ci étant actuellement stables avec Restify.

Les tests ont été effectués sur un environnement Linux (Ubuntu 14.04 LTS). 
Afin d'installer l'application, nous allons:
- installer NodeJS (commandes 1 à 4)
- installer Redis Client et Serveur (commandes 5 et 6)
- installer Restify (commande 7)
- cloner le fichier server.js (commandes 8 et 9)
- lancer le serveur (commande 10)
Pour cela, il faut rentrer les commandes suivantes (nous utiliserons le PPA de Chris Lea):

sudo apt-get install python-software-properties python g++ make
sudo add-apt-repository ppa:chris-lea/node.js
sudo apt-get update
sudo apt-get install nodejs
npm install redis
sudo apt-get install redis-serveur
npm install restify
git clone https://loicmarie@bitbucket.org/loicmarie/api-rest.git
cd api-rest

Si ce n'est pas déjà le cas on lance le serveur Redis avec la commande:
sudo service redis-server start

Enfin, on lance le script avec la commande:
node server.js


Il est possible de tester les methodes avec le fichier client.js, en ouvrant un nouveau terminal et en lançant la commande:

node client.js
